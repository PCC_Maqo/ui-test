#ifndef UI_LABEL
#define UI_LABEL
#include "Ui_Base.h"

typedef struct ui_label {
    ui_preamble

    char* text;
} UiLabel;

UiLabel* Label_New(UiFocusFrame* menu, char* text);
void Label_Free(UiLabel* l);

#endif /* UI_LABEL */
