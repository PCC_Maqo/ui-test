#ifndef UI_TYPES
#define UI_TYPES

#include "Ui_Button.h"
#include "Ui_Label.h"

#define mqui_as_button(a) ((UiButton*)(a))
#define mqui_as_label(a) ((UiLabel*)(a))

#endif /* UI_TYPES */
