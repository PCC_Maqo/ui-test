#ifndef UI_BUTTON
#define UI_BUTTON
#include "Ui_Base.h"

typedef struct ui_button {
    ui_preamble

    void* texture_normal;   // Background texture
    void* texture_hover;    // Background texture
    void* texture_click;    // Background texture
    void* texture_disable;  // Background texture
    char* text;
} UiButton;

UiButton* Button_New(UiFocusFrame* menu, char* text);
void Button_Free(UiButton*);

#endif /* UI_BUTTON */
