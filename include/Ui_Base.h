#ifndef UI_BASE
#define UI_BASE

enum ui_type {
    UI_TYPE_NONE,
    UI_TYPE_LABEL,
    UI_TYPE_BUTTON
};

#define ui_preamble          \
enum ui_type type;           \
void (*on_confirm)();        \
void (*on_hold_left)();      \
void (*on_hold_right)();     \
struct ui_element* up;       \
struct ui_element* down;     \
struct ui_element* left;     \
struct ui_element* right;    \
struct ui_element* next;     /*right bumper to tab right */ \
struct ui_element* previous; /*left bumper to tab left */   \

typedef struct ui_element {
    ui_preamble
} UiElement;

typedef struct ui_focus_frame {
    void (*on_cancel)();
    struct ui_element* focused;
    struct ui_element** elements; // stretchy
} UiFocusFrame;



UiFocusFrame* Ui_GetTopFocus();
UiFocusFrame* Ui_PopFocus();
UiFocusFrame* Ui_PushFocus(UiFocusFrame* focus);

void Ui_CloseTop();

#endif /* UI_BASE */
