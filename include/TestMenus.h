#ifndef TEST_MENUS
#define TEST_MENUS
#include "Ui_Base.h"

UiFocusFrame* MakeMainMenu();
UiFocusFrame* MakeArcadeMenu();
UiFocusFrame* MakeVersusMenu();
UiFocusFrame* MakeReplaysMenu();
UiFocusFrame* MakeOptionsMenu();
void cb_back();

#endif /* TEST_MENUS */
