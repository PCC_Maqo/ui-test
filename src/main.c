/*
Undefine this if running on windows CMD, or potentially if using a unix-y shell
but don't have termios. You will also have to press enter for every input, so
consider changing `case 10: // Return` to something like `case z:`
*/
#define CLEARSCREEN

#include "stdio.h"
#include "stretchy_buffer.h"
#include "termios.h"
#include "TestMenus.h"
#include <unistd.h>
#include <string.h>
#include "Ui_Types.h"


#ifdef CLEARSCREEN
struct termios old_tio, new_tio;
void noecho(){
	/* get the terminal settings for stdin */
	tcgetattr(STDIN_FILENO,&old_tio);

	/* we want to keep the old setting to restore them a the end */
	new_tio=old_tio;

	/* disable canonical mode (buffered i/o) and local echo */
	new_tio.c_lflag &=(~ICANON & ~ECHO);

	/* set the new settings immediately */
	tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
}

void yesecho(){
	tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
}
#endif //CLEARSCREEN

void PrintMenu(UiFocusFrame* menu){
    
    // Will (Should) only occur when quiting
    if(!menu)
        return;
    else{
        printf("%p\n", menu);
    }
    
    #ifdef CLEARSCREEN
    printf("\033c");
    #endif
    
    for(int i = 0; i < sb_count(menu->elements); i++){
        UiElement* current = menu->elements[i];
        switch(current->type){

            case UI_TYPE_BUTTON:
                if (menu->focused == current){
                    printf("> ");
                }
                printf("%s\n", mqui_as_button(current)->text);
                break;

            case UI_TYPE_LABEL:
                printf("%s\n", mqui_as_label(current)->text);
                break;

            case UI_TYPE_NONE:
            default:
                exit(EXIT_FAILURE);
                break;
        }
    }   
    printf("\n");
}

int main (char* args[]){

    #ifdef CLEARSCREEN
    noecho();
    #endif

    MakeMainMenu();

    char in;
    UiFocusFrame* menu = Ui_GetTopFocus();
    do {
        PrintMenu(menu);

        in = getchar();

        switch (in){
            case 'w':
                if (menu->focused->up)
                    menu->focused = menu->focused->up;
                break;

            case 's':
                if (menu->focused->down)
                    menu->focused = menu->focused->down;
                break;

            case 'y':
            case 10: // Return
                if (menu->focused->on_confirm)
                    (*menu->focused->on_confirm)();
                break;
            
            case 'n':
            case 8:   // Delete
            case 127: // Backspace
                Ui_CloseTop();
                break;
        }

        menu = Ui_GetTopFocus();        
    } while(menu);

    #ifdef CLEARSCREEN
    yesecho();
    #endif

    return 0; 
}
