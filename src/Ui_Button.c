#include "Ui_Button.h"
#include "stretchy_buffer.h"
#include <string.h>

UiButton* Button_New(UiFocusFrame* menu, char* text){
    UiButton* butt = malloc(sizeof(UiButton));
    butt->text = malloc(strlen(text) * sizeof(char) + 1);
    butt->type = UI_TYPE_BUTTON;
    strcpy(butt->text, text);
    sb_push(menu->elements, (UiElement*)butt);
    return butt;
}

void Button_Free(UiButton* b){
    free(b->text);
    free(b);
}