#include "TestMenus.h"
#include <stdlib.h>
#include "Ui_Types.h"

#pragma region Button Callbacks
void cb_arcade(){
    MakeArcadeMenu();
}

void cb_versus(){
    MakeVersusMenu();
}

void cb_replays(){
    MakeReplaysMenu();
}

void cb_options(){
    MakeOptionsMenu();
}

void cb_quit(){
    Ui_CloseTop();
}

void cb_back(){
    Ui_CloseTop();
}
#pragma endregion

UiFocusFrame* MakeArcadeMenu(){
    UiFocusFrame* menu = malloc(sizeof(UiFocusFrame));
    menu->elements = 0;

    UiElement* title = (UiElement*)Label_New(menu, "---Arcade: Select Your Character---");
    UiElement* butt_a = (UiElement*)Button_New(menu, "Ryu");
    UiElement* butt_b = (UiElement*)Button_New(menu, "Ken");

    UiElement* butt_back = (UiElement*)Button_New(menu, "back");

    butt_a->down = butt_b;

    butt_b->up = butt_a;
    butt_b->down = butt_back;

    butt_back->on_confirm = cb_back;    

    menu->focused = butt_a;
    return Ui_PushFocus(menu);
}

UiFocusFrame* MakeVersusMenu(){
    UiFocusFrame* menu = malloc(sizeof(UiFocusFrame));
    menu->elements = 0;

    UiElement* title = (UiElement*)Label_New(menu, "---Versus: Select Your Character---");
    UiElement* butt_a = (UiElement*)Button_New(menu, "Ryu");
    UiElement* butt_b = (UiElement*)Button_New(menu, "Ken");
    
    UiElement* butt_back = (UiElement*)Button_New(menu, "back");

    menu->focused = butt_a;
    butt_a->down = butt_b;

    butt_b->up = butt_a;
    butt_b->down = butt_back;
    
    butt_back->on_confirm = cb_back;    

    return Ui_PushFocus(menu);
}

UiFocusFrame* MakeReplaysMenu(){
    UiFocusFrame* menu = malloc(sizeof(UiFocusFrame));
    menu->elements = 0;

    UiElement* title = (UiElement*)Label_New(menu, "---Replays---");
    UiElement* butt_a = (UiElement*)Button_New(menu, "Ry x Ke 2021-12-28-13:24");
    UiElement* butt_b = (UiElement*)Button_New(menu, "Ry x Ke 2021-12-28-13:26");
    UiElement* butt_back = (UiElement*)Button_New(menu, "back");

    butt_a->down = butt_b;

    butt_b->up = butt_a;
    butt_b->down = butt_back;

    butt_back->on_confirm = cb_back;    

    menu->focused = butt_a;
    return Ui_PushFocus(menu);
}

UiFocusFrame* MakeOptionsMenu(){
    UiFocusFrame* menu = malloc(sizeof(UiFocusFrame));
    menu->elements = 0;

    UiElement* title = (UiElement*)Label_New(menu, "---Options---");
    UiElement* butt_a = (UiElement*)Button_New(menu, "Fullscreen");
    UiElement* butt_back = (UiElement*)Button_New(menu, "back");

    butt_a->down = butt_back;

    butt_back->up = butt_a;
    butt_back->on_confirm = cb_back;    

    menu->focused = butt_a;
    return Ui_PushFocus(menu);
}


UiFocusFrame* MakeMainMenu(){
    char c = 'A';
    UiFocusFrame* menu = malloc(sizeof(UiFocusFrame));
    menu->elements = 0;

    UiElement* title = (UiElement*)Label_New(menu, "---Main---");
    UiElement* butt_arcade  = (UiElement*)Button_New(menu, "Arcade");
    UiElement* butt_versus  = (UiElement*)Button_New(menu, "Versus");
    UiElement* butt_replays = (UiElement*)Button_New(menu, "Replays");
    UiElement* butt_options = (UiElement*)Button_New(menu, "Options");
    UiElement* butt_quit    = (UiElement*)Button_New(menu, "quit");
    
    butt_arcade->down = butt_versus;
    butt_arcade->on_confirm = cb_arcade;    

    butt_versus->up = butt_arcade;
    butt_versus->down = butt_replays;
    butt_versus->on_confirm = cb_versus;    
    
    butt_replays->up = butt_versus;
    butt_replays->down = butt_options;
    butt_replays->on_confirm = cb_replays;    

    butt_options->up = butt_replays;
    butt_options->down = butt_quit;
    butt_options->on_confirm = cb_options;
    
    butt_quit->up = butt_options;
    butt_quit->on_confirm = cb_quit;

    menu->focused = butt_arcade;
    return Ui_PushFocus(menu);
}