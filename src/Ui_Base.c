#include "Ui_Base.h"
#include "stretchy_buffer.h"
#include "Ui_Types.h"

UiFocusFrame** _ui_focus_stack = 0;

UiFocusFrame* Ui_GetTopFocus(){
    return sb_count(_ui_focus_stack) ? sb_last(_ui_focus_stack) : 0;
}

UiFocusFrame* Ui_PopFocus(){
    return sb_count(_ui_focus_stack) ? sb_pop(_ui_focus_stack) : 0;
}

UiFocusFrame* Ui_PushFocus(UiFocusFrame* focus){
    return sb_push(_ui_focus_stack, focus);
}

void Ui_CloseTop(){
    UiFocusFrame* frame = Ui_PopFocus();
    for(int i = 0; i < sb_count(frame->elements); i++){
        // free textures
        // free element
        switch(frame->elements[i]->type){
            case UI_TYPE_BUTTON:
                Button_Free((UiButton*) frame->elements[i]);
                break;
            case UI_TYPE_LABEL:
                Label_Free((UiLabel*)frame->elements[i]);
                break;
        }

    }
    sb_free(frame->elements);
    free(frame);
}
 