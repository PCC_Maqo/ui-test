#include "Ui_Label.h"
#include "stretchy_buffer.h"
#include <string.h>

UiLabel* Label_New(UiFocusFrame* menu, char* text){
    UiLabel* lab = malloc(sizeof(UiLabel));
    lab->text = malloc(strlen(text) * sizeof(char) + 1);
    lab->type = UI_TYPE_LABEL;
    strcpy(lab->text, text);
    sb_push(menu->elements, (UiElement*)lab);
    return lab;
}

void Label_Free(UiLabel* l){
    free(l->text);
    free(l);
}